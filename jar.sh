#!/usr/bin/env bash

sudo apt-get install protobuf-compiler -y

cd erhai-jvm 
mvn install -DskipTests
cd erhai-jvm-all
mvn package
rm ../../erhai-app/deployment/zeus-jvm-all-0.6.5-jar-with-dependencies.jar
cp target/erhai-jvm-all-0.6.5-jar-with-dependencies.jar ../../erhai-app/deployment
mvn clean
cd ..
mvn clean