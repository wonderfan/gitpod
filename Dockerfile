FROM adoptopenjdk:8u262-b10-jdk-hotspot-bionic as erhai-base
RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update \
    && apt-get install -y file --fix-missing

FROM adoptopenjdk:8u262-b10-jdk-hotspot-bionic as builder
RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update \
    && apt-get install -y wget git build-essential --fix-missing

ADD . /chain
WORKDIR /chain/erhai-app
RUN wget https://studygolang.com/dl/golang/go1.16.3.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.16.3.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin
ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn
ENV CGO_CFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
ENV CGO_LDFLAGS="-L$JAVA_HOME/jre/lib/amd64/server/ -ljvm -Wl,-rpath,$JAVA_HOME/jre/lib/amd64/server/"
RUN make build-linux

FROM erhai-base
VOLUME /chain
WORKDIR /erhai-app
COPY --from=builder /chain/erhai-app/build /chain/
RUN mv /chain/hcd /usr/local/bin/
COPY --from=builder /chain/erhai-app/deployment/start.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start.sh
RUN mkdir -p /root/go/pkg/mod/github.com/'!cosm!wasm'/wasmvm@v1.0.0-beta/api/
COPY --from=builder /chain/erhai-app/deployment/libwasmvm.so /root/go/pkg/mod/github.com/'!cosm!wasm'/wasmvm@v1.0.0-beta/api/
RUN hcd init test --chain-id="test" --home /root/.hcd
COPY --from=builder /chain/erhai-app/deployment/*.jar /root/.hcd
STOPSIGNAL SIGTERM
EXPOSE 26656 26657 26660 1317 8545
CMD ["/usr/local/bin/start.sh"]
