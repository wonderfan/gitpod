#!/usr/bin/env bash

# setting and install packages
set -e
docker pull weiliy/gmssl

# bootstrap chain
testdir=mytest
if [[ -d ${testdir} ]]; then
    rm -rf ${testdir}
fi

mkdir ${testdir} && cd ${testdir}
cp ../erhai-app/build/hcd .
./hcd init mychain --home mychain --chain-id mychain
./hcd keys add alice --home mychain
./hcd add-genesis-account alice 10000000000000stake --home mychain
cp ../erhai-app/deployment/erhai-jvm-all-0.6.5-jar-with-dependencies.jar mychain
mkdir certs && cd certs
docker run --rm -w "/root" -v "$(pwd):/root" weiliy/gmssl gmssl ecparam -genkey -name sm2p256v1 -text -out ca.key
printf '\n\n\n\n\n\n\n\n\n' | docker run -i --rm -w "/root" -v "$(pwd):/root" weiliy/gmssl gmssl req -new -key ca.key -out ca.csr
docker run --rm -w "/root" -v "$(pwd):/root" weiliy/gmssl gmssl x509 -req -days 365 -in ca.csr -signkey ca.key -out caCert.crt 
cp ../mychain/config/node.key .
printf '\n\n\n\n\n\n\n\n\n' | docker run -i --rm -w "/root" -v "$(pwd):/root" weiliy/gmssl gmssl req -new -key node.key -out node.csr
docker run --rm -w "/root" -v "$(pwd):/root" weiliy/gmssl gmssl x509 -req -days 365 -in node.csr -CA caCert.crt -CAkey ca.key -out node.crt -CAcreateserial
sudo chmod +r node.crt 
cp node.crt ../mychain/config/
cd ..

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    cacert=$(base64 -w 0 certs/caCert.crt)
    nodecert=$(base64 -w 0 certs/node.crt)

elif [[ "$OSTYPE" == "darwin"* ]]; then
    cacert=$(cat certs/caCert.crt | base64)
    nodecert=$(cat certs/node.crt | base64)
fi

SED_EXT=
if [ "$(uname)" == "Darwin" ]; then
    SED_EXT="''"
fi

sed -i $SED_EXT "s/\"RootCert\": \"\"/\"RootCert\": \"${cacert}\"/g" mychain/config/genesis.json
sed -i $SED_EXT "s/\"cert\": \"\"/\"cert\": \"${nodecert}\"/g" mychain/config/genesis.json
#./zeusd start --home mychain > chain.log 2>&1 &
./hcd start --home mychain
#sleep 5s
#cd ..
#bash cases.sh
