#!/usr/bin/env bash

for name in erhai-*; do
    rm -rf $name/.git
done

for mod in mod-*; do
    rm -rf $mod/.git
done