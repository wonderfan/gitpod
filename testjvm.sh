hcd tx jvm deploy-contract "BatchStorageContract:1.0" BatchStorageContract.class --from admin --chain-id erhai --home node0 -b block --yes

hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["mytest","800000"]}' --from admin --chain-id erhai --home node0 -b block --yes
watch hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["mytest","500000"]}' --from admin --chain-id erhai --home node0 -b block --yes
while true; do
    hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["atest","200000"]}' --from admin --chain-id erhai --home node0 -b block --yes
    echo ">>>>> start new transaction"
    #sleep 1s 
done

for n in {1..100}; do
    hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["ctest","2000000"]}' --from admin --chain-id erhai --home node0 -b block --yes
    echo ">>>>> start new transaction $n"
done

sudo find -name "jvm_config.yaml" -exec sed -i "s/5000000/500000000/g" {} \;
sudo find -name "jvm_config.yaml" -exec sed -i "s/10000000/10000000000/g" {} \;
sudo find -name "jvm_config.yaml" -exec cat {} \;

cp ./main/erhai-jvm/erhai-jvm-contract/target/classes/com/loong/erhai/jvm/contract/BatchStorageContract.class main/deploy/fake-cluster/clusterConfig/

dlv core ./erhai-app/build/hcd ./deploy/native/core.11110

gdb hcd core.11110

GOTRACEBACK=crash hcd start --home sm2


# native mode 
hcd tx jvm deploy-contract "BatchStorageContract:1.0" BatchStorageContract.class --from alice --chain-id mychain --home sm2 -b block --yes
hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["mytest","8000000"]}' --from alice --chain-id mychain --home sm2 -b block --yes

while true; do
    hcd tx jvm execute-contract "BatchStorageContract:1.0" "setData" '{"args":["mytest","10000000"]}' --from alice --chain-id mychain --home sm2 -b block --yes
    echo ">>>>>>> next transaction"
    sleep 2s;
done

sudo find clusterConfig/ -iname config.toml -exec sed -i "157s/10/100/g" {} \;
sudo find clusterConfig/ -iname config.toml -exec grep timeout_broadcast_tx_commit {} \;

docker-compose -f docker-compose.yml start
docker-compose logs -f

GOTRACEBACK=crash ./main

docker run --rm -v "$(pwd):/root" --privileged --ulimit core=-1 --security-opt seccomp=unconfined app

docker run -it --rm -v "${PWD}:/root" app

docker build --tag app .