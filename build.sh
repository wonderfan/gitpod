#!/usr/bin/env bash

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # echo "Y" | sdk install java 8.0.292-zulu
    # sudo apt-get install -y jq
    # sudo apt-get install -y uuid-runtime
    export CGO_CFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
    export CGO_LDFLAGS="-L$JAVA_HOME/jre/lib/amd64/server/ -ljvm -Wl,-rpath,$JAVA_HOME/jre/lib/amd64/server/"    
elif [[ "$OSTYPE" == "darwin"* ]]; then
    export CGO_CFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/darwin -w"
    export CGO_LDFLAGS="-L$JAVA_HOME/jre/lib/server/ -ljvm -Wl,-rpath,$JAVA_HOME/jre/lib/server/"
fi

# make build
cd erhai-app
if [[ -e "build/hcd" ]]; then
    rm build/hcd
fi
make build
