set -x

cd erhai-jvm

for repo in zeus-jvm-*; do
    name=${repo//zeus/erhai}
    mv $repo $name
done

for mod in erhai-jvm-*; do
    if [ -d $mod/src/main/java/com/zhigui/zeus ]; then
        mkdir -p $mod/src/main/java/com/loong/erhai
        mv $mod/src/main/java/com/zhigui/zeus/jvm $mod/src/main/java/com/loong/erhai
        rm -rf $mod/src/main/java/com/zhigui
    fi
    if [ -d $mod/src/test ]; then
        rm -rf $mod/src/test
    fi
done

find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zeus-jvm/erhai-jvm/g'
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/com.zhigui.zeus/com.loong.erhai/g' 
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/com.zhigui/com.loong/g' 
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zeus/erhai/g'
