#!/usr/bin/env bash

go build -gcflags=all="-N -l"

dlv exec ./hcd -- start --home mychain

gdb --args ./hcd start --home mychain

./build/benchd perform --rpc-addr="http://localhost:1317" \
--chain-id testnet \
--broadcast-mode BROADCAST_MODE_ASYNC \
--algo sm2 \
--mnemonic="lend believe chronic coconut kind visit furnace sing taste paddle regular wrestle leopard behave mammal answer priority mechanic ski copy wish reduce dish iron" \
--coin-denom stake \
--amount 1 \
--contract-path="http://xian-develop-chengdu.oss-cn-chengdu.aliyuncs.com/tmp/SimpleStorageContract.class" \
--contract-id="simple:1.0" \
--contract-method set \
--generate-accs 5 \
--per-task-txs 10000 \
--send-token-ratio 10 \
--await-time 100

docker run --rm -it -v "${PWD}/node2:/root/.hcd" --entrypoint bash 0fe56ca33636

docker run --rm -it -v "${PWD}/node3:/root/.hcd" --entrypoint bash 0fe56ca33636
