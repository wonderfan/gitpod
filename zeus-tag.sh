#!/usr/bin/env bash

# tag every project and push into repository

tagname="v0.2.0"

projects[0]="zeus-app"
projects[1]="zeus-chain"
projects[2]="zeus-db"
projects[3]="zeus-jvm"
projects[4]="mod-evm"
projects[5]="mod-wasm"
projects[6]="mod-jvm"
projects[7]="mod-tbft"
projects[8]="mod-ibc"
projects[9]="mod-perm"
projects[10]="mod-upgrade"
projects[11]="mod-feemarket"
projects[12]="mod-archive"
projects[13]="mod-gov"
projects[14]="zeus-sdk-go"
projects[15]="go-sdk-proxy"

for project in "${projects[@]}"; do
  echo $project
  cd $project
  git tag -a $tagname -m "new tag $tagname"
  git push origin $tagname
  cd ..
done
