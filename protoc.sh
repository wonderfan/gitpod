#!/usr/bin/env bash

sudo apt-get install -y protobuf-compiler

go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway@v1.16.0
go install github.com/cosmos/cosmos-proto/cmd/protoc-gen-go-pulsar@latest
go install github.com/regen-network/cosmos-proto@latest



