#!/usr/bin/env bash

set +x

if [ -d mirror ]; then
    rm -rf mirror
fi

mkdir mirror

for name in ccb-test/zeus-*; do
    cp -R $name mirror
done

for name in ccb-test/mod-*; do
    cp -R $name mirror
done
cp -R ccb-test/go-sdk-proxy mirror

cp build.sh mirror
cp chain-gm.sh mirror
cp chain.sh mirror
cp sdk.sh mirror
cp git.sh mirror
