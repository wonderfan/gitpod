#!/usr/bin/env bash

function key() {
    ssh-keygen -C threewebcode
    cat ~/.ssh/id_rsa.pub
}

function ssh() {
    cp ssh/* ~/.ssh/
    ls -al ~/.ssh
}

function repo() {
  echo $1
  cd $1
  git config --local user.name threewebcode
  git config --local user.email magestore@outlook.com
}

function create_tsp() {
    npm init typescript-project;
    npx typescript-starter;
}

function node() {
  # curl -sLO https://github.com/MystenLabs/sui/releases/download/mainnet-v1.1.0/sui
  # curl -sLO https://github.com/MystenLabs/sui/releases/download/sui-v1.1.1/sui
  # curl -sLO https://github.com/MystenLabs/sui/releases/download/testnet-v1.2.0/sui
  curl -sLO https://github.com/MystenLabs/sui/releases/download/devnet-v1.3.0/sui
  chmod +x sui
  mv sui $(dirname $(which cargo))
  which sui  
}

function gas() {
  curl --location --request POST 'https://faucet.testnet.sui.io/gas' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "FixedAmountRequest": {
          "recipient": "0x84c123fcd0e19ddd994c22f21d3e2074fe2b6f5fe9fb9c8871ae6eaf8a8b7931"
      }
  }'
}

function publish() {
  sui client publish --gas-budget 3000000000 --skip-dependency-verification --skip-fetch-latest-git-deps --json $1;
}

function import() {
  sui keytool import "$1" ed25519;
}

function switch() {
  sui client switch --address $1;
}

function upgrade() {
  sui client upgrade --json --gas-budget 3000000000 --skip-dependency-verification --skip-fetch-latest-git-deps --upgrade-capability $1 $2
}

$@
