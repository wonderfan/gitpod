#!/usr/bin/env bash
set -x 

for repo in zeus-*; do
    name=${repo//zeus/erhai}
    mv $repo $name
    find "$name" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zhigui-projects/loong-projects/g' 
    find "$name" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|loong-projects/zeus|loong-projects/erhai|g'
    sed -i 's/zeus/erhai/g' "$name/go.mod"
done

for mod in mod-*; do   
    find "$mod" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zhigui-projects/loong-projects/g' 
    find "$mod" -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|loong-projects/zeus|loong-projects/erhai|g'
    sed -i 's/zeus/erhai/g' "$mod/go.mod"
done
mv erhai-app/zeusd erhai-app/hcd
sed -i 's/zeusd/hcd/g' erhai-app/hcd/main.go
sed -i '97 s/zeus/hcd/g' erhai-app/app/app.go
sed -i '186 s/zeusd/hcd/g' erhai-app/app/app.go
sed -i '5 s/ZeusGenesis/hcd/' erhai-app/Makefile
sed -i '6 s/zeusd/hcd/' erhai-app/Makefile
sed -i '11 s/zeusd/hcd/g' erhai-app/Makefile
sed -i '49 s/zeusd/hcd/' erhai-app/hcd/cmd/root.go
sed -i '50 s/zeus/erhai/' erhai-app/hcd/cmd/root.go
sed -i '62 s/Zeus/erhai/' erhai-app/hcd/cmd/testnet.go
sed -i '72 s/zeusd/hcd/' erhai-app/hcd/cmd/testnet.go
sed -i '36 s/zeus/erhai/g' erhai-chain/types/address.go
sed -i '122,126 s/Zeus/erhai/g' erhai-chain/server/start.go
sed -i '120,124 s/zeus/erhai/g' erhai-chain/crypto/keys/sm2/keys.pb.go
sed -i '122,126 s/zeus/erhai/g' erhai-chain/crypto/keys/ed25519/keys.pb.go

find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zhigui_projects_zeus/loong_projects_erhai/g' 
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zhiguiprojects/loongprojects/g' 
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's/zeuschain/erhaichain/g'
find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zhigui/zeus-app|loong/erhai-app|g'
find mod-jvm -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zhigui/zeus|loong/erhai|g'
find mod-jvm -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zhigui.zeus|loong.erhai|g'
#find mod-jvm -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'
mv erhai-chain/proto/zeus erhai-chain/proto/erhai
#find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|zeus|erhai|g'
#find . -type f -not -path '*/\.*' -print0 | xargs -0 sed -i 's|Zeus|Erhai|g'

find erhai-jvm -name "ContractCostMethodAdaptor.java" -print0 | xargs -0 sed -i '11 s|\.|/|g'

